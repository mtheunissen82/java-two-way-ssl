# Java Two Way SSL
This is a small script to demonstrate the working of two way SSL (mutual authentication) with Apache HTTPClient.

## Usage
Package: `$ mvn package`  
Execute: `$ mvn exec:java -D exec.mainClass=com.marc.app.App`
