package com.marc.app;

import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;

import java.io.IOException;

import org.jline.builtins.Completers;
import org.jline.reader.*;

public class TermIO {
    private static TermIO instance;
    private Terminal terminal;
    private LineReader reader;

    private TermIO() throws IOException {
        try {
            TerminalBuilder builder = TerminalBuilder.builder();
            this.terminal = builder.build();

            this.reader = LineReaderBuilder.builder().terminal(terminal).terminal(terminal)
                    .completer(new Completers.FileNameCompleter()).build();

        } catch (Exception e) {
            System.out.println("Error while initializing TermIO");
            throw e;
        }
    }

    public static TermIO instance() {
        try {
            if (TermIO.instance == null) {
                TermIO.instance = new TermIO();
            }

        } catch (Exception e) {
            System.exit(1);
        }

        return TermIO.instance;
    }

    public void print(String message) {
        this.terminal.writer().print(message);
        this.terminal.flush();
    }

    public void println(String message) {
        this.terminal.writer().println(message);
        this.terminal.flush();
    }

    public void printErrorAndExit(String message) {
        this.terminal.writer().println(message);
        this.terminal.flush();
        System.exit(1);
    }

    public LineReader getReader() {
        return this.reader;
    }
}
