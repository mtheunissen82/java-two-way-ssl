package com.marc.app;

import java.security.KeyStore;
import java.io.FileInputStream;
import java.io.File;

import org.apache.commons.validator.routines.UrlValidator;

import com.marc.app.TwoWaySSLTest;
import com.marc.app.TermIO;

public class App {
    public static void main(String[] args) {
        File keystoreFile = null;
        FileInputStream keystoreIs = null;
        KeyStore keystore = null;
        String password = null;
        String endpoint = null;

        UrlValidator urlValidator = new UrlValidator(new String[] { "http", "https" });

        TermIO termio = TermIO.instance();
        termio.println("Welcome to PoC (Apache) HTTPClient mutual (2-way SSL) authentication");

        do {
            String keystoreFilePath = termio.getReader().readLine("Enter path to pfx file> ");
            keystoreFilePath = keystoreFilePath.trim();
            keystoreFile = new File(keystoreFilePath);

            if (!keystoreFile.exists()) {
                termio.println("File not found... try again");
            }

            if (keystoreFile.exists() && !keystoreFile.canRead()) {
                termio.printErrorAndExit("File is not readable");
            }
        } while (!keystoreFile.exists());

        password = termio.getReader().readLine("Enter password> ", '*');
        password = password.trim();

        try {
            keystoreIs = new FileInputStream(keystoreFile);
        } catch (Exception e) {
            termio.printErrorAndExit("Error creating FileInputStream");
        }

        try {
            keystore = KeyStore.getInstance("pkcs12");

            keystore.load(keystoreIs, password.toCharArray());
            termio.println("Keystore successfully instantiated");
        } catch (Exception e) {
            termio.printErrorAndExit("Keystore could not be instantiated");
        }

        do {
            endpoint = termio.getReader().readLine("Enter endpoint URL> ");
            endpoint = endpoint.trim();

            if (!urlValidator.isValid(endpoint)) {
                termio.println("Invalid format URL given... try again");
            }
        } while (!urlValidator.isValid(endpoint));

        try {
            TwoWaySSLTest test = new TwoWaySSLTest();
            test.execute(keystore, password, endpoint);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
